---
title: 攻破Java的Map集合
date: 2020-11-13 14:38:05
tags: Java
categories: 学习笔记
---

# 前言

上回说到，我们与面试官进行了一波Set推手实力互探。双方都发现对面似乎有点东西。

![0dc1efc76cfd3b6fef270ef5e534e9e](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/0dc1efc76cfd3b6fef270ef5e534e9e.jpg)

接下来就要进行更为血腥的Map强攻环节。那必然招招见血，式式带伤啊。

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/241809F3.gif)

<!--more-->

面试官：Map集合用过没？用过哪些呢？

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/2424B0D7.jpg)

我：那必然是用过的，用的最多的主要是HashMap、LinkedHashMap、TreeMap、ConcurrentHashMap。当然Map集合的实现类有很多，就不一一列举了。

面试官：哦，那介绍下HashMap的底层数据结构吧。

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/2421BE92.jpg)

我：现在我们主要用的都是JDK1.8+的版本了，底层是“数组+链表+红黑树”组成实现的。JDK1.8之前是由“数组+链表”组成实现的。

![image-20201113151433349](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/image-20201113151433349.png)

面试官：那为什么1.8以后要改成“数组+链表+红黑树”的形式实现呢？

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/242DB81F.png)

我：主要是为了提升散列表在Hash碰撞冲突严重时（也就是链表过长时）的查询性能，也就是链表查询性能是O(n),而红黑树书O(logn)。

面试官：那什么时候会使用链表，什么时候会使用红黑树呢？

我：对于插入，默认情况下是使用的链表，当一个索引位置的的节点在新增后链表长度达到了9个也就是超过了链表长度阈值8，并且此时数组长度大于等于64时，就会触发链表节点转换为红黑树节点；反之如果数组长度小于64则不会触发红黑树转换，因为此时的数据量还比较小。

面试官：为什么链表转红黑树的阈值是8？

我：这个应该是作者的经验之谈，应该是在时间与空间的角度考虑过后，综合权衡的结果。

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/243B6E8F.gif)

面试官：那转换为红黑树之后就会一直保持是红黑树的状态了吗？

我：（<span style="color:red;">这厮还想坑我，给我挖坑，还好我知道</span>）其实不是的，当红黑树节点少于6，那么就会转回链表。

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/2440BBDB.gif)

面试官：（<span style="color:red;">小伙子还是年轻了呀，你以为我只是挖这么浅的坑嘛</span>）那为什么转回链表阈值用的是6而不是继续采用的前面所说的8呢？

![c74715d783ba6f7d4fa89fe36c77e40](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/c74715d783ba6f7d4fa89fe36c77e40.png)

我：（<span style="color:red;">卧槽，原来在这等我呐。</span>）如果我们设置多于8个转红黑树，少于8个转链表，那在节点个数在8左右徘徊时，势必会造成频繁的转换操作，就会造成性能的消耗。

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/244ADAFC.png)

面试官：（<span style="color:red;">有点东西，小菜鸡</span>）那HashMap有哪些重要的属性，分部是用来做什么的？

我：除了存储数据的table数组以为，HashMap还有以下几个比较重要的属性：

1. size：用来存储HashMap的节点个数
2. threshold：扩容阈值，当HashMap的节点达到该值时，触发扩容
3. loadFactor：负载因子，扩容阈值=容量*负载因子

面试官：（<span style="color:red;">我要开始认真了，准备接招吧。哈撒给</span>）threshold除了用于存放扩容阈值还有其他作用吗？

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/2457B0C0.jpg)

我：（<span style="color:red;">开始认真了嘛，面对疾风吧</span>）在我们新建HashMap对象时，threshold会被用来存放初始化时的容量。跟其他集合类似，HashMap也是直到我们第一次插入数据时才会对table进行初始化，避免不必要的空间浪费。

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/24567F74.jpg)

面试官：那HashMap的初始化容量是多少？HashMap的容量有什么限制吗？

我：默认初始容量为16。HashMap的容量必须是2的N次方，HashMap会根据我们传入的容量数值计算一个大于等于该容量的最小的2的N次方的数值。比如传入9，则会自动计算为16.

面试官：![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/245F0FE5.gif)

你这个结论是怎么得出的呢？

我：根据源码中的tableSizeFor方法可以得出，

```java
static final int tableSizeFor(int cap) {
    int n = cap - 1;
    n |= n >>> 1;
    n |= n >>> 2;
    n |= n >>> 4;
    n |= n >>> 8;
    n |= n >>> 16;
    return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
}
```

 ![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/24615F92.gif)

面试官：就这段代码来解释一下你刚刚的结论

我：我们先跳过第一行，看下面的几行代码。首先我们来解释下几个符号的意思

> 解释|=之前我们先解释下或运算（|）
>
> 参加运算的两个对象，按二进制位进行“**或**”运算。
>
> 运算规则：0|0=0；  0|1=1；  1|0=1；  1|1=1；
>
> "|="这个符号我们平时写代码比较少写到，但是我们经常会写到+=，其实他们的写法含义是差不多的，属于一种简写，a|=b 其实就是a=a|b 的意思。

> ">>>"无符号右移，通俗的讲就是将一个数字除以2的n次方取整数值。

接下里我们来解释下这几行代码的含义

> 那我们先来看第二行代码， n |= n >>> 1; 根据我们刚刚的解释可以得出这行代码的含义就是n右移一位再与n或，翻译成白话文解释首先将数字n转换为二进制，然后将得到的二进制最高位1向右移一位，等同于除以2保留整数。然后在于n进行或。
>
> >苍白的文字解释肯定有点难以理解，我们举个例子来说明：比如说我们传入了一个数字7进来通过第一行代码我们得到n为6。6转换为二进制是0110。
> >
> >第一步：将6（0110）向右移1位得到3（0011）；
> >
> >第二步：将6与3进行或计算得到7（0111）；怎么得到的呢看下面
> >
> >​	0110
> >
> >|   0011
> >
> >=  0111 
> >
> >从上述结果得出无符号右移1位在于原数或操作后我们将低位的0变成了1

> 那我们接下来看第三行代码，代码的含义其实跟第二行是一样的只不过一个位移1位一个位移2位的区别，我们直接解释运算过程
>
> >第一步：将7（0111）向右移2位得到1（0001）；
> >
> >第二步：将7与1进行或计算得到7（0111）；
> >
> >​	0111
> >
> >|   0001
> >
> >=  0111 

接下来位移4、8、16位就不一步一步解释了因为得到的结果都是一样的。其实根据上述我们不难发现，我们的运算最终的结果是将某个数字的二进制从最高位1开始的后续位补齐1，那么最终的结果就是得到某个数字的最贴近的2的N次方-1的数字。比如说我们刚刚说的7得到的结果就自然是7了。那至于为什么要在开始减1，就是为了在输入数字刚好是2的N次方的数值时，避免得到的数值时输入的数值的2倍的大小而浪费空间。

面试官：（<span style="color:red;">可以啊，说的我都听懂了</span>）那为什么HashMap的容量必须是2的N次方呢？

我：（<span style="color:red;">这个问题，百度得到的结果都很数学化，涉及大量的二进制的相关知识，我这里就我自己的理解，转换为更白话文的形式解释一下</span>）首先我们知道，HashMap插入数据时并不是顺序插入，而是通过Hash算法进行计算要插入数据的索引位置，那么为了更好的避免数据存放的冲突性，更加平均的分配数组容器中的元素，最好的方式是采用取模运算来进行分配。那么我们举个例子，我们插入一个数据，数据的key的经过hash计算得出的code值是70，那么我们传统计算就是70%(n-1)得出我们的索引位置。那么如果我们不追求性能效率的极致的话其实这样做其实就可以了。但是作为一门企业级的编程语言，而且作为一个底层级的数据结构，追求极致这是必然的事情。那么计算机的最底层是二进制数据，而最贴近二进制计算的运算符就是`与或非位移`等运算。那么如何用`与或非位移`等运算替换`取模`运算呢。答案是强制要求我们的n的值为2的N次方，这样呢就能做到说n-1的值转换为2进制时 低位数都为1。这样与HashCode值进行位与运算的效果就等同于取模计算。这样呢把取模换成了位与运算，效率肯定就显著提高了！但是这也属于牺牲空间换时间的一种做法。

面试官：（<span style="color:red;">666，手动点赞</span>）说说HashMap的数据插入流程吧？

我：一图胜万语：

<img src="https://gitee.com/cheneyjoo/hexoblog/raw/master/images/HashMapPut.png" alt="HashMapPut" style="zoom:200%;" />

面试官：你刚刚说到HashMap插入数据第一步就是计算key的hash值，计算hash值作为一个散列表最重要的一步，能说说HashMap里面的Hash算法是怎么实现的嘛？

我：调用Java底层的hashcode 算法与得到的hashcode的二进制位的高16位进行异或运算，求得最终的hash值。

```Java
static final int hash(Object key) {
    int h;
    return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
}
```

面试官：为什么要与hashcode的高16位进行异或运算？

我：主要是为了减少hash冲突几率，让元素分布的更平均。  可以看出这句代码的把key的hashcode进行右移了16位等于把高区的16位移动到了低区，然后在于原来的hashcode进行异或运算，做到尽可能的把一个key的二进制的高低位特征进行混合，达到尽可能的减少hash冲突碰撞的几率。

面试官：说说扩容（resize）的流程吧？

我：再赐你一副图

<img src="https://gitee.com/cheneyjoo/hexoblog/raw/master/images/HashMapResize%20.png" alt="HashMapResize " style="zoom:200%;" />

面试官：我看你这幅图在处理红黑树和链表的时候都有通过判断（e.hash&oldCap）==0来定位新表索引位置，这是为什么？

我：这个是在JDK1.8做的优化，因为扩容后新表的n-1转换二进制后只比老表的n-1转换二进制在高位多了一个1。所以在计算新表索引位置时只取决于新表的高位多出的这一个1,而这一位的值刚好就等于oldCap,也就是因为只取决于这一位，所以只会存在两种情况

>1.(e.hash & oldCap) == 0 ，则新表索引位置为“原索引位置”
>
>2.(e.hash & oldCap) != 0，则新表索引位置为“原索引 + oldCap 位置”。

面试官：HashMap是线程安全的嘛？

我：不是，HashMap在并发情况下会存在数据覆盖，遍历的同时进行修改会抛出ConcurrentModificationException异常问题。

面试官：说说其他你用过的其他Map，都一般什么情况会使用他们？

我：

>CocurrentHashMap:需要保证线程安全时会考虑使用它
>
>LinkedHashMap：当取值时需要保证顺序的时候会使用
>
>TreeMap：当需要自定义排序的时候使用

面试官：嗯，Map就问这么多吧，小伙子回去等复试通知。我们组长面你。看你今天表现还行，给你透露下，我们组长喜欢问多线程。

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/29C33FF6.jpg)

我：（<span style="color:red">这叼毛终于完事了。</span>）好的！

![img](https://gitee.com/cheneyjoo/hexoblog/raw/master/images/29C4E420.jpg)