---
title: Java控制台程序选择文件
date: 2020-10-22 16:51:45
tags: Java
categories: 学习笔记

---

```java
import java.util.*;
import java.io.*;
import javax.swing.JFileChooser;
public class ReadFileUsingJFileChooser {
    /**
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        JFileChooser jfc=new JFileChooser();
        if(jfc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
            File file=jfc.getSelectedFile();
            Scanner input=new Scanner(file);
            while(input.hasNext()){
                System.out.println(input.nextLine());
            }
            input.close();
        }
        else
            System.out.println("No file is selected!");
    }

}
```

