---
title: Java中String、StringBuffer、StringBuilder的区别与实现
date: 2020-10-8 14:20:43
tags: Java
categories: 学习笔记
---
# 前言
无论我们使用哪种编程语言相信大家用的最多的就是字符串这个类。因为其通用性强，包容性强，容错率高。不管数据本身是什么数据结构，你将它转换为String类型终归不会出错。而且在跨平台，跨编程语言的数据交互上String往往会是首选。当我们不太能确定来源的数据类型时，定义String接收再进行接下来的数据类型的判断校验，通常是习惯的做法。但是我们使用了如此之久的String类型，其内部实现与我们常用的几个拼接String字符串的类，我们是否都了解呢，知晓其区别呢。接下来我们就将String、StringBuffer、StringBuilder这三个类区别与底层的实现和面试常问的问题进行一个汇总。
<!--more-->
## 底层实现

#### 字符串常量池

​		在了解String的底层实现之前，我们得先知道JVM是如何处理新生成的字符串。首先我们要先搞懂字符串常量池这个概念，常量池是Java的一项技术，八种技术类型中除了Float和Double都实现了常量池技术。这项技术从字面上其实很容易理解，把经常用的数据存放到某块内存中，避免频繁的数据创建于销毁，实现数据共享，提升系统性能。

​		我们先通过三行代码来对常量池进行初步的讲解与认识：

```java
public static void main(String[] args){
	String s1="hello";
	String s2=new String("hello");
	System.out.println(s1 == s2);  //false
}
```

​		首先我们来看看第一行代码String s1="hello";其底层做了什么。直接使用双引号声明赋值字符串的方式是我们最常用的一种方式，它在执行后其底层的实现原理是JVM首先会到字符串常量池中查找该字符串是否已经存在。存在直接返回该引用，否则则会在字符串常量池中创建该字符串对象，然后返回其引用地址。如：代码中JVM首先会到字符串常量池中寻找是否存在“hello”的对象，发现没有后会在字符串常量池中创建“hello”的字符串对象内存地址为（0x0001）。最后把字符串对象地址返回给s1变量。

​		然后我们再看看第二行代码String s2=new String("hello");其底层做了什么。使用new String()的方式一般用的会少一些，与直接双引号赋值相比，表面上看都是创建了一个String的字符串对象，实际其底层还是有很大的差距的。采用new关键字创建字符串对象时，JVM首先在字符串常量池中查找是否存在“hello”对象，如果已经存在呢，就不在去字符串常量池中创建“hello”这个对象了，而是直接去堆内存中创建一个“hello”字符串对象，并返回其引用地址。如果不存在，那么就会首先在字符串常量池中创建一个“hello"对象，然后再去堆内存中创建”hello“对象。所以s2指向的不是字符串常量池中的引用，而是堆内存中的。

​		由上面的底层解析可以得出为什么第三句比较会是false，我们首先得记住Java中==比较的是地址，equals比较的是内容。上面两个声明一个地址指向的是字符串常量池中的，一个是堆内存中的。所以自然不会相等。

​		这里也会在很多公司面试的时候作为面试题提问，最多的就是new String 创建了多少个对象。看完上面的内容其实大家应该都有了答案，这里不管大家回答一个还是两个其实都是不准确的，准确的应该是一或者两个，根据情况的不同创建的对象个数也不一样。这里面试官主要考验的是大家对String对象底层的构建的一个认知度，是很容易犯错的点。

​		这里单独说一个String类下的Intern()方法，之所以在这里单独说这个API因为这个方法不常用，但是做面试题的时候经常会就见到。Intern()在<深入理解Java虚拟机>一书中解释道:String.intern()是一个Native方法,它的作用是:如果字符常量池中已经包含一个等于此String对象的字符串,则返回常量池中字符串的引用,否则,将新的字符串放入常量池,并返回新字符串的引用。我的理解就是返回调用者在字符串常量池的引用地址，通过下面代码应该更容易理解这个方法的含义。

```Java
public class Test {
	public static void main(String argv[])
	{
		String s1 = "HelloWorld";
		String s2 = new String("HelloWorld");
		String s3 = "Hello";
		String s4 = "World";
		String s5 = "Hello" + "World";
		String s6 = s3 + s4;
		
		System.out.println(s1 == s2);//s1指向的字符串常量池的地址，s2指向的是堆内存的地址 所以为 false
		System.out.println(s1 == s5);//s1指向的字符串常量池的地址，s5虽然是拼接但是JVM会自动判断拼接后的字符串信息，也是判断的常量池的地址，true
		System.out.println(s1 == s6);//这个比较容易出错，这种写法等于s6=new String(s3+s4);所以地址不会相等 为false
		System.out.println(s1 == s6.intern());//因为s6创建的时候也会在字符串常量池中创建一个对象，所以比较字符串常量池地址是s6是字符串常量池的地址是存在的所以为true
		System.out.println(s2 == s2.intern());//比较的是字符串常量池地址与堆内存地址,所以为false
	}
}
```

#### String

​		String类是从JDK1.0开始就有的类，继承Object类，实现了Serializable,Comparable<String>, CharSequence三个接口。通过查看String源码我们可以得知String其底层是存储在一个char类型的常量数组中的，所以String对象是不可变的并且是以char数组的形式进行存储的。

```java
public final class String
    implements java.io.Serializable, Comparable<String>, CharSequence {
    /** The value is used for character storage. */
    private final char value[];

```
#### StringBuffer

​		StringBuffer是从JDK1.0开始就有的类，继承AbstractStringBuilder类，实现了Serializable, Comparable<StringBuffer>, CharSequence三个接口。通常用于拼接字符串操作，是线程安全的所以在多线程中不需要做特殊的同步处理。通过查看StringBuffer源码我们可以得知StringBuffer其底层也是存储在char类型的数组中的，但是不同的是StringBuffer中的char类型数组并没有被final修饰，说明他是允许修改的。而且我们默认声明一个StringBuffer对象时，会给我们的char数组初始化16个长度。所以StringBuffer是可变的。其中从Java11开始也引入了CompareTo的方法

```Java
public final class StringBuffer
    extends AbstractStringBuilder
    implements java.io.Serializable, Comparable<StringBuffer>, CharSequence
{

    /**
     * A cache of the last value returned by toString. Cleared
     * whenever the StringBuffer is modified.
     */
    private transient char[] toStringCache;

    /** use serialVersionUID from JDK 1.0.2 for interoperability */
    static final long serialVersionUID = 3388685877147921107L;

    /**
     * Constructs a string buffer with no characters in it and an
     * initial capacity of 16 characters.
     */
    public StringBuffer() {
        super(16);
    }
```

​		既然StringBuffer是可变的，那么如果拼接的字符串长度超过了16个长度，StringBuffer是如何处理的呢。我们继续看StringBuffer类中Append方法的源码,下面是JDK15的源码，发现跟JDK11的有点不太一样，但是细研究了一下发现算法没变，只是重构了expandCapacity方法，提取出来了几个方法，算法依然跟以前一样，新插入数据长度超过现有长度，如果没有超出两倍，那么就扩容为当前的长度*2+2，否则就直接使用超出长度。

```java
 /**
     * Appends the specified string to this character sequence.
     * <p>
     * The characters of the {@code String} argument are appended, in
     * order, increasing the length of this sequence by the length of the
     * argument. If {@code str} is {@code null}, then the four
     * characters {@code "null"} are appended.
     * <p>
     * Let <i>n</i> be the length of this character sequence just prior to
     * execution of the {@code append} method. Then the character at
     * index <i>k</i> in the new character sequence is equal to the character
     * at index <i>k</i> in the old character sequence, if <i>k</i> is less
     * than <i>n</i>; otherwise, it is equal to the character at index
     * <i>k-n</i> in the argument {@code str}.
     *
     * @param   str   a string.
     * @return  a reference to this object.
     */
    public AbstractStringBuilder append(String str) {
        if (str == null) {
            return appendNull();
        }
        int len = str.length();
        ensureCapacityInternal(count + len);
        putStringAt(count, str);
        count += len;
        return this;
    }
	 /**
     * For positive values of {@code minimumCapacity}, this method
     * behaves like {@code ensureCapacity}, however it is never
     * synchronized.
     * If {@code minimumCapacity} is non positive due to numeric
     * overflow, this method throws {@code OutOfMemoryError}.
     */
    private void ensureCapacityInternal(int minimumCapacity) {
        // overflow-conscious code
        int oldCapacity = value.length >> coder;
        if (minimumCapacity - oldCapacity > 0) {
            value = Arrays.copyOf(value,
                    newCapacity(minimumCapacity) << coder);
        }
    }
	 /**
     * Returns a capacity at least as large as the given minimum capacity.
     * Returns the current capacity increased by the current length + 2 if
     * that suffices.
     * Will not return a capacity greater than
     * {@code (MAX_ARRAY_SIZE >> coder)} unless the given minimum capacity
     * is greater than that.
     *
     * @param  minCapacity the desired minimum capacity
     * @throws OutOfMemoryError if minCapacity is less than zero or
     *         greater than (Integer.MAX_VALUE >> coder)
     */
    private int newCapacity(int minCapacity) {
        int oldLength = value.length;
        int newLength = minCapacity << coder;
        int growth = newLength - oldLength;
        int length = ArraysSupport.newLength(oldLength, growth, oldLength + (2 << coder));
        if (length == Integer.MAX_VALUE) {
            throw new OutOfMemoryError("Required length exceeds implementation limit");
        }
        return length >> coder;
    }
	 /**
     * Calculates a new array length given an array's current length, a preferred
     * growth value, and a minimum growth value.  If the preferred growth value
     * is less than the minimum growth value, the minimum growth value is used in
     * its place.  If the sum of the current length and the preferred growth
     * value does not exceed {@link #MAX_ARRAY_LENGTH}, that sum is returned.
     * If the sum of the current length and the minimum growth value does not
     * exceed {@code MAX_ARRAY_LENGTH}, then {@code MAX_ARRAY_LENGTH} is returned.
     * If the sum does not overflow an int, then {@code Integer.MAX_VALUE} is
     * returned.  Otherwise, {@code OutOfMemoryError} is thrown.
     *
     * @param oldLength   current length of the array (must be non negative)
     * @param minGrowth   minimum required growth of the array length (must be
     *                    positive)
     * @param prefGrowth  preferred growth of the array length (ignored, if less
     *                    then {@code minGrowth})
     * @return the new length of the array
     * @throws OutOfMemoryError if increasing {@code oldLength} by
     *                    {@code minGrowth} overflows.
     */
    public static int newLength(int oldLength, int minGrowth, int prefGrowth) {
        // assert oldLength >= 0
        // assert minGrowth > 0

        int newLength = Math.max(minGrowth, prefGrowth) + oldLength;
        if (newLength - MAX_ARRAY_LENGTH <= 0) {
            return newLength;
        }
        return hugeLength(oldLength, minGrowth);
    }
```

#### StringBuilder

​	StringBuffer是从JDK1.5才开始加入的类，继承AbstractStringBuilder类，实现Serializable`, `Appendable`, `CharSequence`, `Comparable<StringBuilder>三个接口。通常用于拼接字符串操作。其实StringBuilder与StringBuffer底层原理几乎是一样的，只是一个是线程安全的，一个不是而已，在性能上自然是StringBuilder会更快一些，因为StringBuffer在拼接字符串的时候需要频繁的加锁，释放锁肯定会有性能影响。我们可以从源码中看到这两个类的区别。StringBuffer类中的append方法都加了synchronized修饰。所以在线程同步这块StringBuffer是不需要担心的，而StringBuilder在多线程下就需要注意线程同步的问题。扩容等都与StringBuffer无任何差别，都是继承于AbstractStringBuilder，采用的该类的方法。

```Java
    //StringBuffer
    @Override
    public synchronized StringBuffer append(Object obj) {
        toStringCache = null;
        super.append(String.valueOf(obj));
        return this;
    }

    @Override
    @HotSpotIntrinsicCandidate
    public synchronized StringBuffer append(String str) {
        toStringCache = null;
        super.append(str);
        return this;
    }
    
    //StringBuilder
    @Override
    public StringBuilder append(Object obj) {
        return append(String.valueOf(obj));
    }

    @Override
    @HotSpotIntrinsicCandidate
    public StringBuilder append(String str) {
        super.append(str);
        return this;
    }
```



## 区别

其实上面已经提到了String，StringBuffer与StringBuilder的区别了，这里在进行总结一下

### 可变与不可变

String类是不可变的，因为内部实现的char数组使用了final进行修饰，定义为常量

StringBuffer与StringBuilder是可变的，内部的char数组没有使用final进行修饰，并且在AbstractStringBuilder父类实现了动态扩容的方法。

### 线程的安全与不安全

StringBuffer的内部实现方法都使用了synchronized进行修饰，所以其方法具有线程安全的特性，也是因为这个特性，StringBuffer在具备线程安全的同时牺牲了性能。

StringBuilder的内部方法并未使用synchronized进行修饰，所以其方法是不具备线程安全的。所以在性能方面StringBuilder具有其优势。

## 共同点

StringBuilder与StringBuffer有公共父类AbstractStringBuilder(抽象类)。

抽象类与接口的其中一个区别是：抽象类中可以定义一些子类的公共方法，子类只需要增加新的功能，不需要重复写已经存在的方法；而接口中只是对方法的申明和常量的定义。